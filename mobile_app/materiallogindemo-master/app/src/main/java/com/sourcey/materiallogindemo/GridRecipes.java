package com.sourcey.materiallogindemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.sourcey.materiallogindemo.FrescoGridRecipes.*;
import com.sourcey.materiallogindemo.FrescoGridRecipes.MainActivity;
import com.sourcey.materiallogindemo.R;

public class GridRecipes extends AppCompatActivity {

    Integer[] imageIDs = {
            R.drawable.bread,
            R.drawable.snacks,
            R.drawable.diet,
            R.drawable.soups,
            R.drawable.vegiie,
            R.drawable.sweets,
            R.drawable.squid,
            R.drawable.specialdays,
            R.drawable.side,
            R.drawable.seafood,
            R.drawable.salads,
            R.drawable.meat,
            R.drawable.fish,
            R.drawable.ricepasta
    };
    String[] categories = {
            "Pao",
            "Entradas e Petiscos",
            "diet",
            "soups",
            "vegetarian",
            "sweets",
            "Polvo",
            "Dias Especiais",
            "side",
            "seafood",
            "salads",
            "Carne",
            "Peixe",
            "RiceAndPasta"
    };
    //later i need to come her and change this values
    /**String[] categories = {
            "bread",
            "snacks",
            "diet",
            "soups",
            "vegetarian",
            "sweets",
            "squid",
            "Dias Especiais",
            "side",
            "seafood",
            "salads",
            "Carne",
            "Peixe",
            "RiceAndPasta"
    };*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_grid);
        Fresco.initialize(GridRecipes.this);
        ListViewCompat gridView = (ListViewCompat) findViewById(R.id.gridview);
        gridView.setAdapter(new ImageAdapter(this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id)
            {
               /** Toast.makeText(getBaseContext(),
                        "pic" + (position + 1) + " selected",
                        Toast.LENGTH_SHORT).show();*/
                Intent intent = new Intent(getApplicationContext(), RemoteImageActivity.class);
                intent.putExtra("email",getIntent().getStringExtra("email"));
                intent.putExtra("super",getIntent().getStringExtra("super"));
                intent.putExtra("cat", categories[position]);
                startActivityForResult(intent, Activity.RESULT_OK);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });
    }
    @Override
    public void onBackPressed() {
        finishActivity();
    }

    private void finishActivity() {
        this.finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }
    public class ImageAdapter extends BaseAdapter
    {
        private Context context;

        public ImageAdapter(Context c)
        {
            context = c;
        }

        //---returns the number of images---
        public int getCount() {
            return imageIDs.length;
        }

        //---returns the ID of an item---
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        //---returns an ImageView view---
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ImageView imageView;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            if (convertView == null) {
                imageView = new ImageView(context);
                imageView.setLayoutParams(new GridView.LayoutParams(width, (width/2) ));
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }
            imageView.setImageResource(imageIDs[position]);
            return imageView;
        }
    }
}

package com.sourcey.materiallogindemo.Utils;

import android.support.annotation.Nullable;
import android.util.Log;


import com.birbit.android.jobqueue.CancelReason;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;


/**
 * Created by psimoes on 5/28/17.
 */

public class BroadCastJob extends Job {
    public static final int PRIORITY = 1;
    private DatagramSocket socket;


    public BroadCastJob(){
        super(new Params(PRIORITY).requireNetwork().persist());
        Log.e("before ","atum");
    }

    @Override
    public void onAdded() {

    }
    @Override
    public void onRun() throws Throwable {
        byte[] buf = new byte[256];
        socket = new DatagramSocket(9000);
        // receive request

        while(true){
            Log.e("broadcast"," broadcast");
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            String a = new String(buf, 0, packet.getLength());
            Log.e("received",a);

           // if(new String(buf, 0, packet.getLength()).equalsIgnoreCase("Ritzy")){
                Log.e("sending","send");
                InetAddress address = packet.getAddress();
                int port = packet.getPort();
                Log.e("received",address.getHostAddress());
                Log.e("received",packet.getPort()+"");
                packet = new DatagramPacket(buf, buf.length, address, port);
                socket.send(packet);
            //}
        }
    }
    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount,
                                                     int maxRunCount) {

        return RetryConstraint.createExponentialBackoff(runCount, 1000);
    }
    @Override
    protected void onCancel(@CancelReason int cancelReason, @Nullable Throwable throwable) {
        // Job has exceeded retry attempts or shouldReRunOnThrowable() has decided to cancel.
    }
}

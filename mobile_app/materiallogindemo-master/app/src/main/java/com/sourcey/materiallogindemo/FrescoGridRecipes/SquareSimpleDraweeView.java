package com.sourcey.materiallogindemo.FrescoGridRecipes;

/**

 */

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

public class SquareSimpleDraweeView extends SimpleDraweeView
{
    public SquareSimpleDraweeView(Context context)
    {
        super(context);
    }

    public SquareSimpleDraweeView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SquareSimpleDraweeView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }

}
package com.sourcey.materiallogindemo.Utils;

import android.support.annotation.Nullable;
import android.util.Log;

import com.birbit.android.jobqueue.CancelReason;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sourcey.materiallogindemo.DownloadFilesTask;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by psimoes on 20-05-2017.
 */
public class PostList extends Job {
    public static final int PRIORITY = 1;
    private ArrayList<String> ingredients;
    private ArrayList<String> ing_post=new ArrayList<>();
    private String email;
    public PostList(ArrayList<String> recipes,String email) {
        // This job requires network connectivity,
        // and should be persisted in case the application exits before job is completed.
        super(new Params(PRIORITY).requireNetwork().persist());
        this.ingredients=ingredients;
        this.email=email;

    }
    @Override
    public void onAdded() {

    }
    @Override
    public void onRun() throws Throwable {
        int counter = 0;
        Iterator<String> it = ingredients.iterator();
        while(it.hasNext()){
            String[] splitted = it.next().split(" ");
            for(int i = 0; i < splitted.length; i++){
                ing_post.add(splitted[i]);
            }
        }

        try {
            JSONObject params = new JSONObject();
            params.put("srv", "recipes_add");
            params.put("email", email);

            Log.e("SUPER",email);

            Gson gson=new GsonBuilder().create();
            params.put("ing", gson.toJson(ing_post));

            new DownloadFilesTask().execute(params).get();
        } catch (Exception e) {

        }
    }
    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount,
                                                     int maxRunCount) {

        return RetryConstraint.createExponentialBackoff(runCount, 1000);
    }
    @Override
    protected void onCancel(@CancelReason int cancelReason, @Nullable Throwable throwable) {
        // Job has exceeded retry attempts or shouldReRunOnThrowable() has decided to cancel.
    }
}
package com.sourcey.materiallogindemo.FrescoGridRecipes;


public class ImageItem {
    final int drawableId;
    final String imagePath;
    final String imageName;
    final String ingredients;
    final String prep;

    ImageItem(int drawableId, String imagePath,String imagename,String ingr,String prep)
    {
        this.drawableId = drawableId;
        this.imagePath = imagePath;
        this.imageName=imagename;
        this.ingredients=ingr;
        this.prep=prep;
    }


}
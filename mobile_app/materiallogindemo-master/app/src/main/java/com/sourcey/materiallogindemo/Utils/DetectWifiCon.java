package com.sourcey.materiallogindemo.Utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by psimoes on 5/13/17.
 */

public class DetectWifiCon {
    public static boolean checkInternetConnection(Context context) {

        ConnectivityManager con_manager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (con_manager.getActiveNetworkInfo() != null
                && con_manager.getActiveNetworkInfo().isAvailable()
                && con_manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}

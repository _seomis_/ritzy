package com.sourcey.materiallogindemo.FrescoPantry;

/**
 * Created by psimoes on 20-05-2017.
 */


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.sourcey.materiallogindemo.R;

import java.util.Collections;
import java.util.List;



public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.IngViewHolder>{

    private static IngredientAdapter instance;
    private List<Ingredient> ings = Collections.emptyList();


    public static IngredientAdapter getInstance(){
        if (instance==null){
            instance = new IngredientAdapter();
        }
        return instance;
    }
    @Override
    public IngViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_ingredient_fresco,parent,false);
        return new IngViewHolder(view);
    }

    @Override
    public void onBindViewHolder(IngViewHolder holder, int position) {



        final Ingredient itemIng = ings.get(position);



        if (itemIng.title != null){
            holder.titleTextView.setText(itemIng.title);
        }

        if (itemIng.subtitle != null){
            holder.subtitleTextView.setText(itemIng.subtitle);
        }

        if (itemIng.image != null){
            final Uri uri = Uri.parse(itemIng.image);
            final Context context = holder.itemView.getContext();
            holder.simpleDraweeView.setImageURI(uri, context);
        }



    }

    @Override
    public int getItemCount() {

        return ings.size();
    }

    public void setIngs(List<Ingredient> ing) {
        this.ings = ing;
        notifyDataSetChanged();
    }


    public static class IngViewHolder extends RecyclerView.ViewHolder{
        public TextView titleTextView;
        public TextView subtitleTextView;
        public SimpleDraweeView simpleDraweeView;
        public IngViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView)itemView.findViewById(R.id.title_textView);
            subtitleTextView = (TextView)itemView.findViewById(R.id.subtitle_textView);
            simpleDraweeView = (SimpleDraweeView)itemView.findViewById(R.id.ings_image);
        }
    }
}




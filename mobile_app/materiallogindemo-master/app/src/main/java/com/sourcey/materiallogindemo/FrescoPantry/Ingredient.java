package com.sourcey.materiallogindemo.FrescoPantry;

/**
 * Created by psimoes on 20-05-2017.
 */

import com.google.gson.annotations.SerializedName;

public class Ingredient {

    @SerializedName("ID") // Une annotation qui indique que ce champ doit être sérialisée à JSON avec la valeur de nom fourni comme clé.
    public final long id;
    @SerializedName("Title")
    public final String title;
    @SerializedName("SubTitle")
    public final String subtitle;
    @SerializedName("Description")
    public final String description;
    @SerializedName("Image")
    public final String image;
    @SerializedName("isbn")
    public final String isbn;

    public Ingredient(long id, String title, String subtitle, String description, String image, String isbn) {
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
        this.description = description;
        this.image = image;
        this.isbn = isbn;
    }
}

package com.sourcey.materiallogindemo.FrescoGridRecipes;

/**
 * Created by Pedro Simoes on 15/5/17.
 * Use Fresco for loading remote images.
 */

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.materiallogindemo.DownloadFilesTask;
import com.sourcey.materiallogindemo.FrescoIndividualRecipe.WelcomeActivity;
import com.sourcey.materiallogindemo.R;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RemoteImageFragment extends Fragment {
    View view;
    private int width;
    MyAdapter adapter;
    GridView gridView;
    RelativeLayout loadingLayout;
    private List<ImageItem> items = new ArrayList<ImageItem>();


    private OkHttpClient client = new OkHttpClient();

    public static RemoteImageFragment newInstance() {
        RemoteImageFragment fragment = new RemoteImageFragment();
        return fragment;
    }

    public RemoteImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels / 4;
        adapter = new MyAdapter(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_remote_image, container, false);
        loadingLayout = (RelativeLayout)view.findViewById(R.id.loading);
        gridView = (GridView) view.findViewById(R.id.gridview);
        gridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        gridView.invalidateViews();
        getRemoteItemList();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
// TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class MyAdapter extends BaseAdapter {
        private LayoutInflater inflater;

        public MyAdapter(Context context) {
            inflater = LayoutInflater.from(context);

        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int i) {
            return items.get(i);
        }

        @Override
        public long getItemId(int i) {
            return items.get(i).drawableId;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final ViewHolder holder;

            if (view == null) {
                view = inflater.inflate(R.layout.gridview_draweeview_item, viewGroup, false);

                holder = new ViewHolder();
                holder.imgQueue = (SquareSimpleDraweeView) view.findViewById(R.id.picture);
                holder.recipename = (TextView) view.findViewById(R.id.recipename);
                holder.imgQueue.setMaxHeight(width);
                holder.imgQueue.setMaxWidth(width);
                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
            }

            final ImageItem item = (ImageItem) getItem(i);
            Uri uri = Uri.parse(item.imagePath);
            holder.imgQueue.setImageURI(uri);
            holder.recipename.setText(item.imageName);

            final Drawable white = getResources().getDrawable( R.drawable.white);

            final View finalView = view;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Drawable highlight = getResources().getDrawable( R.drawable.highlight);

                    if(((RemoteImageActivity)getActivity()).getSelection()) {

                        if(!v.isSelected() ) {
                            finalView.setBackground(highlight);
                            finalView.setSelected(true);
                            ((RemoteImageActivity) getActivity()).getHashMap().put(item.imageName,item.ingredients.replaceAll("\\n"," "));
                            ((RemoteImageActivity) getActivity()).getMyRecipes().put(item.imageName,item.imagePath);

                        }else{
                            finalView.setBackground(white);
                            v.setSelected(false);
                            ((RemoteImageActivity) getActivity()).getHashMap().remove(item.imageName);
                            ((RemoteImageActivity) getActivity()).getMyRecipes().remove(item.imageName);
                        }
                    }
                    else {
                        //save the item info
                        Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                        intent.putExtra("recipename", item.imageName);
                        intent.putExtra("recipeurl", item.imagePath);
                        intent.putExtra("ingredients", item.ingredients);
                        intent.putExtra("prep", item.prep);
                        //((RemoteImageActivity) getActivity()).getHashMap().put(item.imageName, item.ingredients.split("|"));
                        getActivity().startActivity(intent);

                    }
                }
            });
            /**
             * prevent recycled views to show hightlight but contains gives false.. wtf
             */
            if(!((RemoteImageActivity) getActivity()).getHashMap().containsKey(item.imageName)){
                Log.d("imageUrl: "+item.imageName);
                Log.d("checked contains: "+((RemoteImageActivity) getActivity()).getChecked().size());
                finalView.setBackground(white);
            }


            return view;
        }
    }

    public class ViewHolder {
        SquareSimpleDraweeView imgQueue;
        TextView recipename;
    }




    public void getRemoteItemList() {



        String url = "http://193.136.120.145:5919/recipes/"+getActivity().getIntent().getStringExtra("cat");
        Log.d("url: "+url);
        Request request = new Request.Builder()
                .url(url)
                .build();
        final Handler mHandler = new Handler();
        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(final Request request, final IOException e) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                    }
                });
            }

            @Override
            public void onResponse(Response response) throws IOException {
                items.clear();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String result = response.body().string();
                try {

                    // For parsing recipes
                    JSONObject resultObj = new JSONObject(result);
                    JSONArray recipesArray = resultObj.getJSONArray("all");
                    items = new ArrayList<ImageItem>();
                    for (int i = 0, size = recipesArray.length(); i < size; i++){
                        JSONObject aRecipe = recipesArray.getJSONObject(i);
                            String imageUrl = aRecipe.getString("img");
                            String name = aRecipe.getString("name");
                            String ingredients = aRecipe.getString("ingredients_recipe");
                            JSONArray prepa = aRecipe.getJSONArray("prep");


                            String prep = prepa.join("\n");
                            Log.d("imageUrl: "+imageUrl);
                            items.add(new ImageItem(R.drawable.no_media, imageUrl,name,ingredients.replace("|","\n"),prep.replace("\\n","")));
                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        gridView.invalidateViews();
                        loadingLayout.setVisibility(View.GONE);

                    }
                });
            }
        });
    }
}

package com.sourcey.materiallogindemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.birbit.android.jobqueue.JobManager;
import com.sourcey.materiallogindemo.Utils.BroadCastJob;
import com.sourcey.materiallogindemo.Utils.DetectWifiCon;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.Bind;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    private static DetectWifiCon wifi = new DetectWifiCon();

    @Bind(R.id.input_name) EditText _nameText;
    @Bind(R.id.input_user) EditText _userText;
    @Bind(R.id.input_email) EditText _emailText;
    @Bind(R.id.input_country) Spinner _countryText;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.input_reEnterPassword) EditText _reEnterPasswordText;
    @Bind(R.id.btn_signup) Button _signupButton;
    @Bind(R.id.link_login) TextView _loginLink;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void signup() {
        Log.d(TAG, "Signup");
        if (!wifi.checkInternetConnection(this)) {
            Toast.makeText(getApplicationContext(), "No INTERNET!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!validate()) {
            onSignupFailed("");
            return;
        }

        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        final String name = _nameText.getText().toString();
        final String user = _userText.getText().toString();
        final String email = _emailText.getText().toString();
        final String country = _countryText.getSelectedItem().toString();
        final String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        // TODO: Implement your own signup logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        JSONObject success = postToSever(name, user,email,country,password);
                        try {
                            if (success.getString("Content").toCharArray()[1] != 'o'){
                                onSignupFailed(success.getString("Content"));
                            }
                            else
                                onSignupSuccess();
                        } catch (JSONException e) {e.printStackTrace();}


                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    public JSONObject postToSever(String name, String user, String email, String country, String password) {

        JSONObject res = null;
        try {
            JSONObject params = new JSONObject();
            params.put("srv", "signup");
            params.put("name", name);
            params.put("email", email);
            params.put("user", user);
            params.put("pass", password);
            params.put("country", country);

            res = new DownloadFilesTask().execute(params).get();
        } catch (Exception e) {

        }
        return res;
    }
    @Override
    public void onBackPressed() {
        finishActivity();
    }

    private void finishActivity() {
        this.finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }


    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    public void onSignupFailed(String res) {
        Toast.makeText(getBaseContext(), "SignUp failed: " + res, Toast.LENGTH_LONG).show();

        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String user = _userText.getText().toString();
        String email = _emailText.getText().toString();
        String country = _countryText.getSelectedItem().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("at least 3 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (user.isEmpty()) {
            _userText.setError("Enter Valid Username");
            valid = false;
        } else {
            _userText.setError(null);
        }


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (country.isEmpty()) {
            TextView errorText = (TextView)_countryText.getSelectedView();
            errorText.setError("anything here, just to add the icon");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText("Choose Country");
            valid = false;
        } else {
           // _countryText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            _reEnterPasswordText.setError("Password Do not match");
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }
}
package com.sourcey.materiallogindemo;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by daniel on 13-05-2017.
 */

public class DownloadFilesTask  extends AsyncTask<JSONObject, Void, JSONObject> {

    protected void onProgressUpdate(Integer... progress) {
    }


    protected void onPostExecute(Long result) {
    }

    @Override
    protected JSONObject doInBackground(JSONObject[] params) {

        JSONObject param = params[0];

        String srv = "";

        HttpURLConnection urlConnection;

        try {

            String type ="";
            // TODO: adicionar outros modos
            if(param.get("srv").equals("signup")) {
                srv = "http://193.136.120.145:5919/account/register";
                type = "POST";
            }
            if(param.get("srv").equals("login")) {
                srv = "http://193.136.120.145:5919/account/login";
                type = "POST";
            }
            if(param.get("srv").equals("pantry_add")) {
                srv = "http://193.136.120.145:5919/pantry/add";
                type = "POST";
            }
            if(param.get("srv").equals("recipes_add")) {
                srv = "http://193.136.120.145:5919/lists/addrecipes";
                type = "POST";
            }
            param.remove("srv");
            URL url = new URL(srv);

            String requestBody = param.toString();
            Log.e("sent message", requestBody);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(type);
            //urlConnection.setDoInput (true);
            urlConnection.setDoOutput (true);
            //urlConnection.setUseCaches (false);
            urlConnection.setRequestProperty("Content-Type","application/json");
            OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
            writer.write(requestBody);
            writer.flush();
            writer.close();
            outputStream.close();

            JSONObject jsonObject = new JSONObject();
            InputStream inputStream;
            // get stream
            if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
            }
            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String temp, response = "";
            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            // put into JSONObject
            jsonObject.put("Content", response);
            jsonObject.put("Code", urlConnection.getResponseCode());
            jsonObject.put("Message", urlConnection.getResponseMessage());
            //jsonObject.put("Length", urlConnection.getContentLength());
            //jsonObject.put("Type", urlConnection.getContentType());

            return jsonObject;




        }catch (IOException e){
            Log.e("IO", e.toString());
        }catch (JSONException e){
            Log.e("JSON", "catch");

        }
        return null;
    }
}
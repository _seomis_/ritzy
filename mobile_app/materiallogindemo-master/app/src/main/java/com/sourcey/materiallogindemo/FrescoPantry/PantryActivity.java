package com.sourcey.materiallogindemo.FrescoPantry;

/**
 * Created by psimoes on 20-05-2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.sourcey.materiallogindemo.FrescoGridRecipes.ImageItem;
import com.sourcey.materiallogindemo.GridRecipes;
import com.sourcey.materiallogindemo.MainActivity;
import com.sourcey.materiallogindemo.R;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PantryActivity extends AppCompatActivity {

    private static final String DEFAULT_QUERY = "Android";
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String NO_ERROR_VALUE = "0";
    private OkHttpClient client = new OkHttpClient();
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    private List<Ingredient> items = new ArrayList<Ingredient>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.pantry_main);
        RecyclerView IngsRecyclerView = (RecyclerView)findViewById(R.id.ings_recyclerView);
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.ings_swipe_refresh_layout);

        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                getRemoteItemList();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        //Ajouter un Layout Manager a notre recylerView
        IngsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Ajouter notre adapter
        IngsRecyclerView.setAdapter(IngredientAdapter.getInstance());

        //Lancer la requete maintenant
        getRemoteItemList();
    }
    public void getRemoteItemList() {



        String url = "http://193.136.120.145:5919/pantry/get";

        RequestBody body = RequestBody.create(JSON, "{\"email\":"+"\""+getIntent().getStringExtra("email")+"\""+"}");
        com.sourcey.materiallogindemo.FrescoGridRecipes.Log.d("url: "+url);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        final Handler mHandler = new Handler();
        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(final Request request, final IOException e) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                    }
                });
            }

            @Override
            public void onResponse(Response response) throws IOException {
                items.clear();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String result = response.body().string();
                try {

                    // For parsing pantry

                    JSONArray recipesArray = new JSONArray(result);
                    items = new ArrayList<Ingredient>();
                    for (int i = 0, size = recipesArray.length(); i < size; i++){
                        JSONObject anIng = recipesArray.getJSONObject(i);
                        String imageUrl = anIng.getString("url");
                        String name = anIng.getString("ingredient");
                        String price = anIng.getString("price");
                        String qty = anIng.getString("qty");
                        String id = anIng.getString("id");
                        com.sourcey.materiallogindemo.FrescoGridRecipes.Log.d("imageUrl: "+imageUrl);
                        items.add(new Ingredient(12, name, "Quantidade: "+qty, price, imageUrl, ""));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        IngredientAdapter.getInstance().setIngs(items);

                    }
                });
            }
        });
    }
}

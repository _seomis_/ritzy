package com.sourcey.materiallogindemo.FrescoGridRecipes;


/**
 *
 */

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.birbit.android.jobqueue.JobManager;

import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import com.sourcey.materiallogindemo.Jobs.RitzyJobManager;
import com.sourcey.materiallogindemo.MainMenuActivity;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.Utils.PostList;
import com.sourcey.materiallogindemo.Utils.PostRecipe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import cn.pedant.SweetAlert.SweetAlertDialog;


public class RemoteImageActivity extends Activity {

    Fragment remoteImageFragment;
    private static final int REQUEST_CODE = 732;
    private boolean selection = false;
    private JobManager jobManager;

    private TextView tvResults;
    private Map<String,String> mResults = new HashMap<String,String>();
    private Map<String,String> mRecipes = new HashMap<String,String>();
    private static final List<String> checked = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_remote_image);
        remoteImageFragment = RemoteImageFragment.newInstance();
        jobManager = RitzyJobManager.getInstance().getJobManager();
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction
                .replace(R.id.localImageContainer, remoteImageFragment, null)
                .commit();
        final ImageView fabIconNew = new ImageView(this);
        //needed deprecated for lower api's
        fabIconNew.setImageDrawable(getResources().getDrawable(R.mipmap.ritzy_icon));
        final FloatingActionButton rightLowerButton = new FloatingActionButton.Builder(this)
                .setContentView(fabIconNew)
                .build();

        SubActionButton.Builder rLSubBuilder = new SubActionButton.Builder(this);
        ImageView rlIcon1 = new ImageView(this);
        // nao temos tempo para fazer estas
        ImageView rlIcon2 = new ImageView(this);
        //ImageView rlIcon3 = new ImageView(this);
        ImageView rlIcon4 = new ImageView(this);

        rlIcon1.setImageDrawable(getResources().getDrawable(R.mipmap.done));
        //nao temos tempo para fazer estas
        rlIcon2.setImageDrawable(getResources().getDrawable(R.mipmap.list));
        //rlIcon3.setImageDrawable(getResources().getDrawable(R.mipmap.euro));
        rlIcon4.setImageDrawable(getResources().getDrawable(R.mipmap.select));
        rlIcon1.isClickable();
        rlIcon2.isClickable();
        //rlIcon3.isClickable();
        rlIcon4.isClickable();






        // Build the menu with default options: light theme, 90 degrees, 72dp radius.
        // Set 4 default SubActionButtons
        final FloatingActionMenu rightLowerMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(rLSubBuilder.setContentView(rlIcon1).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon2).build())
                //.addSubActionView(rLSubBuilder.setContentView(rlIcon3).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon4).build())
                .attachTo(rightLowerButton)
                .build();

        // Listen menu open and close events to animate the button content view
        rightLowerMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
            @Override
            public void onMenuOpened(FloatingActionMenu menu) {
                // Rotate the icon of rightLowerButton 45 degrees clockwise
                fabIconNew.setRotation(0);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 45);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fabIconNew, pvhR);
                animation.start();

            }

            @Override
            public void onMenuClosed(FloatingActionMenu menu) {
                // Rotate the icon of rightLowerButton 45 degrees counter-clockwise
                fabIconNew.setRotation(45);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 0);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fabIconNew, pvhR);
                animation.start();
            }
        });

        /**
         * select images
         */
        rlIcon4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(!selection) {
                    Toast.makeText(getBaseContext(),
                            "Selection Enabled",
                            Toast.LENGTH_SHORT).show();
                    selection = true;

                }
                else {
                    Toast.makeText(getBaseContext(),
                            "Selection Disabled",
                            Toast.LENGTH_SHORT).show();
                    selection = false;
                }

            }

        });
        rlIcon2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(getHashMap().size()>0)
                    new SweetAlertDialog(RemoteImageActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Add this recipes to favorites?")
                            .setConfirmText("Yes, add!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    jobManager.addJobInBackground(new PostList(new ArrayList<String>(getMyRecipes().values()),getIntent().getStringExtra("email")));
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();

            }

        });
        /**
         * show list!Daniel!
         */
        rlIcon1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(getHashMap().size()>0)
                new SweetAlertDialog(RemoteImageActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Buy Recipes List?")
                        .setContentText("You can add more recipes you know!")
                        .setConfirmText("Yes, I'm going to buy it!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                jobManager.addJobInBackground(new PostRecipe(new ArrayList<String>(getHashMap().values()),getIntent().getStringExtra("email"),getIntent().getStringExtra("super")));

                                sDialog
                                        .setTitleText("Bought!")
                                        .setContentText("Your Pantry has been Updated. Save this List to your Favourite Recipes?")
                                        .setConfirmText("Yes")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog
                                                        .setTitleText("Saved Recipes in Your Lists!")
                                                        .setContentText("Now back to main Menu ;)")
                                                        .setConfirmText("OK")
                                                        .showCancelButton(false)
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                Intent intent = new Intent(getBaseContext(), MainMenuActivity.class);
                                                                intent.putExtra("email",getIntent().getStringExtra("email"));
                                                                startActivity(intent);
                                                                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                                            }
                                                        })
                                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                            }
                                        })
                                        .setCancelText("No")
                                        .showCancelButton(true)
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.cancel();
                                                Intent intent = new Intent(getBaseContext(), MainMenuActivity.class);
                                                intent.putExtra("email",getIntent().getStringExtra("email"));
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                            }
                                        })
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                            }
                        })
                        .show();

            }

        });




    }


    public Map<String,String> getHashMap(){
        return mResults;
    }
    public Map<String,String> getMyRecipes(){
        return mRecipes;
    }
    public List<String> getChecked(){
        return checked;
    }

    public boolean getSelection(){
        return selection;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);

    }



    @Override
    public void onBackPressed() {
        finishActivity();
    }

    private void finishActivity() {
        this.finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

}

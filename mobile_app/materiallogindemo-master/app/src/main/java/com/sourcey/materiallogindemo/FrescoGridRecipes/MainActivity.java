package com.sourcey.materiallogindemo.FrescoGridRecipes;

/**
 * Created by Pedro Simoes 14/05
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.facebook.drawee.backends.pipeline.Fresco;

public class MainActivity extends Activity  {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(MainActivity.this);


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}

module.exports = Object.freeze({
    CATEGORIES: ['frescos_fruta', 'frescos_frutossecos', 'frescos_ervas', 'frescos_vegetais', 'frescos_talho', 'frescos_peixaria', 'frescos_marisco', 'charcutaria', 'mercearia_massa', 'mercearia_oleoeazeite', 'mercearia_feijaoegrao', 'mercearia_temperos', 'mercearia_enlatados', 'mercearia_sopa', 'mercearia_molhos', 'mercearia_snacks', 'mercearia_sobremesas', 'mercearia_bolachas', 'mercearia_açucar', 'mercearia_cereais', 'lacticinios', 'refeições_prontas', 'refeições_sobremesas', 'padaria', 'pastelaria', 'bebida', 'bebida_cafeecha', 'higiene', 'higiene_higienemulher', 'higiene_homem', 'domestico_casa', 'bebe', 'animais', 'papelaria', 'farmacia', 'textil', 'promo', 'all', 'Carne','Peixe','Dias Especiais','Pão, Broas e Bolas','Doces e Sobremesas','Polvo, Choco e Lulas'],
    TAGS:['53 DC 5D A4','90 0C AF B5','7E 97 00 44'],
    auth: '8RjfqtU7OKSeFMwCnQY5t361CJFQMWYQxNooDY0tIBqGyDCgZl9HMtDVLpqI'
});

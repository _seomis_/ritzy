#include <UDPServer.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include <PN532_I2C.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>
#include "utility/debug.h"
#include <IPAddress.h>
#include "utility/socket.h"
#include "UDPServer.h"
// These are the interrupt and control pins
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// These can be any two pins
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
// Use hardware SPI for the remaining pins
// On an UNO, SCK = 13, MISO = 12, and MOSI = 11
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIVIDER); // you can change this clock speed

#define WLAN_SSID       "daniel"           // cannot be longer than 32 characters!
#define WLAN_PASS       "12345678"
// Security can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2
#define WLAN_SECURITY   WLAN_SEC_WPA2

#define IDLE_TIMEOUT_MS  3000      // Amount of time to wait (in milliseconds) with no data 
                                   // received before closing the connection.  If you know the server
                                   // you're accessing is quick to respond, you can reduce this value.

// What page to grab!
#define WEBSITE "http://193.136.120.145"
#define URL "/pantry/decrease"

#define UDP_READ_BUFFER_SIZE 20
#define LISTEN_PORT_UDP 9000
UDPServer udpServer(LISTEN_PORT_UDP);

union ArrayToIp { byte array[4]; uint32_t ip; };
ArrayToIp server = { 145, 120, 136, 193 };

Adafruit_CC3000_Client www;
PN532_I2C pn532_i2c(Wire);
NfcAdapter nfc = NfcAdapter(pn532_i2c);  // Indicates the Shield you are using
// initialize the library with the numbers of the interface pins
//LiquidCrystal lcd(7,8,9,6,11,12);

char payload[180];  // Reserve a char to store the data to send. Account for ~60 bytes per variable. 
char le[4];

IPAddress broadcastIP;
uint16_t broadcastPort = 9000;
Adafruit_CC3000_Client udp;
unsigned long timeout = millis() + 15000;
const char* packetBuffer = "Ritzy";
int n = strlen(packetBuffer);


void setup() {

    Serial.begin(9600);
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);
  // set up the LCD's number of columns and rows: 
  //lcd.begin(16, 2);
  // Print a message to the LCD.
  //lcd.print("hello, wld!");
    Serial.println(F("\nInitializing..."));

  nfc.begin();
    /* Initialise the module CC3000 */
  if (!cc3000.begin()) {
    Serial.println(F("Couldn't begin()! Check your wiring?"));
    while(1);
  }
  
  Serial.print(F("\nAttempting to connect to ")); Serial.println(WLAN_SSID);
  if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
    Serial.println(F("Failed!"));
    while(1);
  } 
  Serial.println(F("Connected!"));
  
  /* Wait for DHCP to complete */
  Serial.println(F("Request DHCP"));
  while (!cc3000.checkDHCP()) {
    delay(100); // ToDo: Insert a DHCP timeout!
  }
  getIpAddress();
  //udpServer.begin();

}



void loop() {
          

  if (nfc.tagPresent()){
    NfcTag tag = nfc.read();
    char tg[12];
    tag.getUidString().toCharArray(tg, 12);
    Serial.println(tg);
    delay(2000);
    sendToServer(tg);       
  }/*
  else{
    sendBroadCast();
    receiveBroadCast();
  }*/
}

void getIpAddress()
{
  uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;
  
  if(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
  {
    Serial.println(F("Unable to retrieve the IP Address!\r\n"));
    return false;
  }
  else
  {
    broadcastIP = ipAddress;
    broadcastIP[0] = 255;
    Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
    Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
    Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
    Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
    Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
    Serial.println();
  }
}

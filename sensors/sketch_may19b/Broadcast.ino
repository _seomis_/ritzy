

void sendBroadCast(){
  
  if(!udp.connected()){
    Serial.println("Connecting to server");
    udp = cc3000.connectUDP(broadcastIP, broadcastPort);
    Serial.println(udp.connected());
  } 
  // n == 650
  if(udp.connected()){
    int cc = udp.write(packetBuffer, n);
    if (cc < 0) { 
      Serial.print("error writing packet: "); Serial.println(n);
    } else if (cc != n) {
      Serial.print("wrote "); Serial.print(cc); Serial.print(" octets, but expected to write "); Serial.println(n);
    }
    Serial.println("Done");
    
    udp.close();
  }
}

boolean receiveBroadCast(){
  Serial.println("Receive");
  
 if (udpServer.available()) {

      char buffer[UDP_READ_BUFFER_SIZE];
      int n = udpServer.readData(buffer, UDP_READ_BUFFER_SIZE);  // n contains # of bytes read into buffer
      //Serial.print("n: "); Serial.println(n);
      if(strcmp(buffer, "Ritzy")){
        return true;
      }
      /*for (int i = 0; i < n; ++i) {
         char c = buffer[i];
         Serial.print("c: "); Serial.println(buffer);
         // ... Do whatever you want with 'c' here ...
      }*/
   }
  return false;
}


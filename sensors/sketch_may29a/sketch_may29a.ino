
#include <PN532_I2C.h>
#include <NfcAdapter.h>
#include <Adafruit_CC3000.h>


// These are the interrupt and control pins
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// These can be any two pins
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
// Use hardware SPI for the remaining pins
// On an UNO, SCK = 13, MISO = 12, and MOSI = 11
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIVIDER); // you can change this clock speed

#define WLAN_SSID       "daniel"           // cannot be longer than 32 characters!
#define WLAN_PASS       "12345678"
// Security can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2
#define WLAN_SECURITY   WLAN_SEC_WPA2

#define IDLE_TIMEOUT_MS  3000      // Amount of time to wait (in milliseconds) with no data 
                                   // received before closing the connection.  If you know the server
                                   // you're accessing is quick to respond, you can reduce this value.

// What page to grab!
#define WEBSITE "193.136.120.145"
#define URL "/pantry/decrease"

union ArrayToIp { byte array[4]; uint32_t ip; };
ArrayToIp server = { 145, 120, 136, 193 };

Adafruit_CC3000_Client www;
PN532_I2C pn532_i2c(Wire);
NfcAdapter nfc = NfcAdapter(pn532_i2c);  // Indicates the Shield you are using

char payload[180];  // Reserve a char to store the data to send. Account for ~60 bytes per variable. 
char le[4];

void setup() {

    Serial.begin(9600);
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);
    Serial.println(F("\nInitializing..."));

  nfc.begin();
    /* Initialise the module CC3000 */
  if (!cc3000.begin()) {
    Serial.println(F("Couldn't begin()! Check your wiring?"));
    while(1);
  }
  
  Serial.print(F("\nAttempting to connect to ")); Serial.println(WLAN_SSID);
  if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
    Serial.println(F("Failed!"));
    while(1);
  } 
  Serial.println(F("Connected!"));
  
  /* Wait for DHCP to complete */
  Serial.println(F("Request DHCP"));
  while (!cc3000.checkDHCP()) {
    delay(100); // ToDo: Insert a DHCP timeout!
  }

}
void loop() {
          

  if (nfc.tagPresent()){
    NfcTag tag = nfc.read();
    char tg[12];
    tag.getUidString().toCharArray(tg, 12);
    Serial.println(tg);
    delay(2000);
    sendToServer(tg);       
  }
}

void sendToServer(char* tg) {

        sprintf(payload,"%s%s",payload, "{\"email\":\"pms@uninova.pt\",\"id\":\"");
        sprintf(payload,"%s%s", payload, tg);
        sprintf(payload,"%s%s", payload, "\",\"supermarket\":\"pingo\"}");
    
        // Get length of the entire payload
        sprintf(le,"%d", strlen(payload));
    
        Serial.println(payload);
        Serial.println(le);
    
        
        if(!www.connected()){
            Serial.println("Connecting to server");
            www = cc3000.connectTCP(server.ip, 5919);
        } 
        if (www.connected()) {
            Serial.println("Test Passed");
            www.fastrprint(F("POST "));
            www.fastrprint(URL);
            www.fastrprint(F(" HTTP/1.1\r\n"));
            www.fastrprint(F("Content-Type: application/json\r\n"));
            www.fastrprint(F("Connection: close\r\n"));
            www.fastrprint(F("Host: "));
            www.fastrprint(WEBSITE); 
            www.fastrprint(F("\r\n"));
            www.fastrprint(F("Content-Length: "));
            www.fastrprint(le);
            www.fastrprint(F("\r\n"));
            www.fastrprint(F("\r\n"));
            www.fastrprint(payload);
            www.fastrprint(F("\r\n"));
            www.println();
            Serial.println("posted");
        }
        else{
            Serial.println("Not Connected");
            return;
        }
        Serial.println(F("-------------------------------------"));
      
      /* Read data until either the connection is closed, or the idle timeout is reached. */ 
        unsigned long lastRead = millis();
        while (www.connected() && (millis() - lastRead < IDLE_TIMEOUT_MS)) {
          while (www.available()) {
            char c = www.read();
            Serial.print(c);
            lastRead = millis();
          }
        }
        www.close();
        Serial.println();
        Serial.println(F("-------------------------------------"));
        delay(300);
        memset(payload, 0, sizeof(payload)); 
    
        //Serial.println(F("\n\nDisconnecting"));
        //cc3000.disconnect();
        delay(3000);  
}


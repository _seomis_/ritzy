# Ritzy

![](images/image.png)

## Table of contents

* [Description](#section_description)
* [Scrappers](#section_scrappers)
* [Server](#section_server)
* [Mobile App](#section_mobile)
* [Sensors](#section_sensors)


<a name='section_scrappers'/>
## Scrappers

1. for pingo doce products
2. for continente products

<a name='section_server'/>
## Server

1. Account managing
2. Recipe Listing
3. Ingredients Listing
4. Shopping Lists

<a name='section_mobile'/>
## Mobile App



<a name='section_sensors'/>
## Sensors
